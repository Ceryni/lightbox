let container = $('.featuredVideos');

if(container.length) {

	// video buttons
	let vidLink = $('.imgLightbox');
	// create the lightbox holder
	let overlay	= $('<div id="lightboxOverlay"></div>');
	// create the close button
	let closeBtn = $('<span class="fa fa-times"></span>');
	// the iframe of the video
	let iframe =  $('.lightframe iframe');
	// the video url
	let url = iframe.attr('src');

	// add the overlay to the body
	$('body').append(overlay);
	// add a close button
	overlay.append(closeBtn);
	// hide it initially
	overlay.hide();

	// When a user clicks on an image
	$(vidLink).on('click', function() {

		// reload the url if it's been removed on a previous click
		iframe.attr('src', url);

		$(this).next('.lightframe')
			.clone()
			.addClass('shown')
			.appendTo(overlay)
			.fadeIn('1000');

		// add a close button
		overlay.append(closeBtn);
		// fade in the overlay
		overlay.fadeIn('1000');

		// on click of the close button
		closeBtn.on('click', () => {

			iframe.attr('src', '');
			// fade out the overlay
			overlay.html('')
			.fadeOut('1000')
			.removeClass('shown');

		});

	});
}